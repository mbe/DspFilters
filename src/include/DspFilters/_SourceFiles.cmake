# $Id:$

set( RelativeDir "include/DspFilters" )
set( RelativeSourceGroup "include\\DspFilters" )

set( DirFiles
	Bessel.h
	Biquad.h
	Butterworth.h
	Cascade.h
	ChebyshevI.h
	ChebyshevII.h
	Common.h
	Custom.h
	Design.h
	Dsp.h
	Elliptic.h
	Filter.h
	Layout.h
	Legendre.h
	MathSupplement.h
	Params.h
	PoleFilter.h
	RBJ.h
	RootFinder.h
	SmoothedFilter.h
	State.h
	Types.h
	Utilities.h
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

