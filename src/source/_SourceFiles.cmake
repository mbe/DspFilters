# $Id:$

set( RelativeDir "source" )
set( RelativeSourceGroup "Source Files" )

set( DirFiles
	Bessel.cpp
	Biquad.cpp
	Butterworth.cpp
	Cascade.cpp
	ChebyshevI.cpp
	ChebyshevII.cpp
	Custom.cpp
	Design.cpp
	Documentation.cpp
	Elliptic.cpp
	Filter.cpp
	Legendre.cpp
	Param.cpp
	PoleFilter.cpp
	RBJ.cpp
	RootFinder.cpp
	State.cpp
	_SourceFiles.cmake
)
set( DirFiles_SourceGroup "${RelativeSourceGroup}" )

set( LocalSourceGroupFiles  )
foreach( File ${DirFiles} )
	list( APPEND LocalSourceGroupFiles "${RelativeDir}/${File}" )
	list( APPEND ProjectSources "${RelativeDir}/${File}" )
endforeach()
source_group( ${DirFiles_SourceGroup} FILES ${LocalSourceGroupFiles} )

